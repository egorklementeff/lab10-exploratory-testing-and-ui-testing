import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MoodleTest {

    @Test
    public void testMoodle() {

        System.setProperty("webdriver.gecko.driver", "/home/egorklementev/lectures/software_quality_and_reliability/driver/geckodriver");

        WebDriver driver = new FirefoxDriver();
        driver.get("https://moodle.innopolis.university/");

        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Innopolis University", driver.getTitle());

        WebElement loginHref = driver.findElement(By.linkText("Log in"));
        loginHref.click();

        new WebDriverWait(driver, 5).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));

        Assert.assertEquals(driver.getCurrentUrl(), "https://moodle.innopolis.university/login/index.php");

        WebElement forgotHref = driver.findElement(By.linkText("Forgotten your username or password?"));
        forgotHref.click();

        new WebDriverWait(driver, 5).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));

        Assert.assertEquals("Forgotten password", driver.getTitle());
        Assert.assertEquals(driver.getCurrentUrl(), "https://moodle.innopolis.university/login/forgot_password.php");

        WebElement usernameInput = driver.findElement(By.id("id_username"));
        usernameInput.sendKeys("12345");

        WebElement searchButton = driver.findElement(By.name("submitbuttonusername"));
        searchButton.click();

        new WebDriverWait(driver, 5).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));

        WebElement continueButton = driver.findElement(By.tagName("button"));
        continueButton.click();

        new WebDriverWait(driver, 5).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));

        Assert.assertEquals("Innopolis University", driver.getTitle());
        Assert.assertEquals(driver.getCurrentUrl(), "https://moodle.innopolis.university/index.php?");

        driver.quit();
    }
}

